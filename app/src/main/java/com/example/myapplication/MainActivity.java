package com.example.myapplication;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.fragment.app.FragmentManager;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener
{
    private FragmentManager fm = getSupportFragmentManager();
    private LinearLayout liaotian,lianxiren, pengyouquan, setting;
    private ImageView lt,lxr,pyq,st;

    private Fragment tab1= new tab1();
    private Fragment tab2 = new tab2();
    private Fragment tab3 = new tab3();
    private Fragment tab4 = new tab4();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initFragment();
        initImageView();
        showFragment(0);

        liaotian.setOnClickListener(this);
        lianxiren.setOnClickListener(this);
        pengyouquan.setOnClickListener(this);
        setting.setOnClickListener(this);
    }

    private void initFragment() {
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.add(R.id.content, tab1);
        transaction.add(R.id.content,tab2);
        transaction.add(R.id.content,tab3);
        transaction.add(R.id.content,tab4);
        transaction.commit();
    }

    private void initImageView() {
        lt = findViewById(R.id.lt);
        lxr = findViewById(R.id.lxr);
        pyq = findViewById(R.id.pyq);
        st = findViewById(R.id.st);

        liaotian = findViewById(R.id.liaotian);
        lianxiren = findViewById(R.id.lianxiren);
        pengyouquan  = findViewById(R.id.pengyouquan);
        setting = findViewById(R.id.setting);
    }

    private void hideFragment(FragmentTransaction transaction) {
        transaction.hide(tab1);
        transaction.hide(tab2);
        transaction.hide(tab3);
        transaction.hide(tab4);
    }

    @Override
    public void onClick(View view) {
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        hideFragment(fragmentTransaction);
        resetImg();
        switch (view.getId()) {
            case R.id.liaotian:
                showFragment(0);
                break;
            case R.id.lianxiren:
                showFragment(1);
                break;
            case R.id.pengyouquan:
                showFragment(2);
                break;
            case R.id.setting:
                showFragment(3);
                break;
            default:
                break;
        }
    }

    private void resetImg() {    //调用灰色的图片，以让点击过后的图片回复原色
        lt.setImageResource(R.drawable.liaotian);
        lxr.setImageResource(R.drawable.lianxiren);
        pyq.setImageResource(R.drawable.pengyouquan);
        st.setImageResource(R.drawable.setting);

    }

    private void showFragment(int i) {    //控制图片颜色的变换，其意义是点击一个图片之后该图片就会变亮
        FragmentTransaction transaction = fm.beginTransaction();
        hideFragment(transaction);
        switch (i) {
            case 0:
                transaction.show(tab1);
                lt.setImageResource(R.drawable.liaotian1);
                break;
            case 1:
                transaction.show(tab2);
                lxr.setImageResource(R.drawable.lianxiren1);
                break;
            case 2:
                transaction.show(tab3);
                pyq.setImageResource(R.drawable.pengyouquan1);
                break;
            case 3:
                transaction.show(tab4);
                st.setImageResource(R.drawable.setting1);
                break;
            default:
                break;
        }
        transaction.commit();
    }
}







